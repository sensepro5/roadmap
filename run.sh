#!/bin/sh

echo "PDFLATEX"

pdflatex SENSE-Roadmap.tex
pdflatex SENSE-Roadmap.tex

echo "BibTex"

bibtex SENSE-Roadmap.aux

echo "PDFLATEX"

pdflatex SENSE-Roadmap.tex

echo "DONE"
