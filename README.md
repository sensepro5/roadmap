## SENSE ROADMAP

---

** Main files **

** SENSE-Roadmap.tex ** Main roadmap LaTex file.

** refs.bib ** BibTex file containing references.

** run.sh ** a bash script to build the document. Uses **pdflatex** and **bibtex**.

---

** To build the document **

** $cd roadmap

** $source run.sh **

